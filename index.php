<?php
/**
 * Если бы были возможны теги a без атрибута href, надо было бы проверять наличие href для однозначной идентификации тега.
 * В рамках тестового задания при получении атрибута href с недефолтным неймспейсом этот неймспейс захардкожен.
 * Класс Normalizer - из библиотеки Intl, она должна быть установлена для корректной работы скрипта.
 */

$options = getopt('p:');
if (empty($options['p'])) {
    throw new Exception('Указан неверный путь к файлу');
}

$pathToXMLFile = $options['p'];
if (!file_exists($pathToXMLFile)) {
    throw new Exception('Файл не существует');
}

$XML = simplexml_load_file($pathToXMLFile);
if ($XML === false) {
    throw new Exception('Некорректный XML в файле');
}

$result = parseXML($XML);

echo 'Суммарное число букв внутри тегов, не включая пробельные символы - '.$result['symbolsWithoutSpaces']."\n";
echo 'Суммарное число букв нормализованного текста внутри тегов, включая и пробелы - '.$result['symbolsWithSpaces']."\n";
echo 'Число внутренних ссылок - '.$result['links']."\n";
echo 'Число битых внутренних ссылок - '.$result['brokenLinks']."\n";

/////////////////////////////////////////

function parseXML(SimpleXMLElement $XML)
{
    static $symbolsWithoutSpaces = 0;
    static $symbolsWithSpaces = 0;
    static $links = 0;
    static $brokenLinks = 0;

    static $ids = [];

    foreach ($XML->children() as $Child /* @var $Child SimpleXMLElement */) {

        foreach ($Child->attributes() as $name => $value) {
            if ($name == 'id') {
                $ids[] = $value;
            }
        }

        $childAsString = (string)$Child;

        if (!empty($childAsString)) {
            $symbolsWithoutSpaces += strlen(str_replace(' ', '', $childAsString)); // суммарное число букв внутри тегов, не включая пробельные символы
            $symbolsWithSpaces += strlen(Normalizer::normalize($childAsString)); // Суммарное число букв нормализованного текста внутри тегов, включая и пробелы
        }

        if ($Child->getName() == 'a') {
            $links++; // Число внутренних ссылок
            foreach ($Child->attributes('l', true) as $name => $value) {
                if ($name == 'href' && !in_array(mb_substr($value, 1), $ids)) {
                    $brokenLinks++; // Число битых внутренних ссылок
                }
            }
        }

        parseXML($Child);
    }

    return [
        'symbolsWithoutSpaces' => $symbolsWithoutSpaces,
        'symbolsWithSpaces' => $symbolsWithSpaces,
        'links' => $links,
        'brokenLinks' => $brokenLinks
    ];
}


